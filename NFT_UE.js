//CIM Enterprises

/**
 * @NApiVersion 2.0
 * @NScriptType UserEventScript
 * @NModuleScope Public
 */
define([],
	function () {

		/**
		 * Function definition to be triggered before record is loaded.
		 *
		 * @param {Object} scriptContext
		 * @param {Record} scriptContext.newRecord - New record
		 * @param {string} scriptContext.type - Trigger type
		 * @param {Form} scriptContext.form - Current form
		 * @Since 2015.2
		 */
		function beforeLoad(scriptContext) {
			scriptContext.form.addPageInitMessage({
				type: 'information',
				title: 'SuiteNFT',
				message: 'Hello SuiteNFT World!'
			});
		}

		return {
			beforeLoad: beforeLoad
		};

	});